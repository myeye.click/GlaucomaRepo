Welcome to GlaucomeRepo

As part of Myeye.click project, GlaucomaRepo is repository of latest scientific publication related to glaucoma. GlaucomaRe analysis the text with state-of-the-art methods in ML, NLP, and build an advanced web interface to interact and use the data.
Myeye.click web site: http://myeye.click

The structure recommended for this repository is as follows:
/GlaucomaRepo/
    README.md
    Contents-metadata.txt  # Defines datasets and projects within the project in a json-ish style
    /Datasets/
        /[NameOfDataset]/
		[... datasets and transformations]
		/Projects-[nameOfProjectForThisDataset]/
            		[... project files and directories]


