

def fromXML():
    import xml.etree.cElementTree as ET
    #Pubmed in medline format:
    url0 = "http://www.ncbi.nlm.nih.gov/pubmed/"
    url1 = "?report=medline&format=text"
    #url1 ="?report=abstract&format=text"

    tree = ET.ElementTree(file='30000.xml')
    for elem in tree.iterfind('IdList/Id'):
        print url0+elem.text+url1

def fromPMIDlist():
    import requests
    c=0
    url0 = "http://www.ncbi.nlm.nih.gov/pubmed/"
    url1 = "?report=medline&format=text"
    fname = "59486.txt"
    with open(fname) as f:
        items = f.readlines()
        for item in items:
            myfile = "records-medline/"+item.strip()+".txt"
            import os.path
            c=c+1
            if os.path.isfile(myfile)==False:
                url = url0+item.strip()+url1
                record = requests.get(url)
                f = open(myfile, "w")
                f.write(record.content)
                f.close()
                print str(c)+") "+item.strip()

###############
## Manual calls:
fromPMIDlist()
