from Bio import Medline
import re, os

'''
with open("myml.txt") as handle:
    records = Medline.parse(handle)
    for record in records:
        print(record['AB'])
'''
noabs=''
abs = ''
mydir = '../records-medline/'
c=0
n=0
for i in os.listdir(mydir):
    if i.endswith(".txt") and c>-1:
        #print '#############'
        print str(c)+'  '+i
        c+=1
        rec = ''
        with open(mydir+i) as handle:
            record = Medline.read(handle)
            if 'AB' in record and 'AU' in record:
                # Add title
                if 'TI' in record:
                    rec += record['TI']
                rec += (' | ').join(record['AU'])
                rec += '\n'

                abs = record['AB']
                # or commnet/uncomment to clean record:
                #abs = re.sub(r'\.|,|-|;|\(|\)','',record['AB'])

                rec += abs
                #print rec
                f = open('../records-medline-with-abs/'+i, "w")
                f.write(rec)
                f.close()
            else:
                n+=1
                noabs +=  i+' '

print 'Total records: '+str(c)
print 'Total no abstract records: '+str(n)
print '('+str(n*100/c)+'%)'
#print noabs

f = open('no-complete-list.txt', "w")
f.write(noabs)
f.close()

'''
results:
Total records: 59486
Total no abstract records: 19567 (32%)
Total records with Authors and Abstract: 39919
'''
