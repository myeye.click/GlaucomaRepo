'''
    this script builds Word2Vec models using gensim. It adapts the tutorial
    http://rare-technologies.com/word2vec-tutorial/

    It reads from a directoory of text files. And generates a model with the desired combination of word2vec algorith parameters.

    For each generated model it saves the moel itself, the resulting tensor, and the list of terms used for that model parameters..
'''

#################################
## define names and parameters:
# raw text directory
datasetDir = '../DATASET-3-records-medline-with-abs-CLEAN/' # absolut or relative to this script

# Define directory to save the generated models, tensors, terms list/
modelDir = '../DATASET-4-word2vec/'

# Parameters for wrod2vec:
windowRange = [1, 2, 4, 8, 16, 32, 64] # min_count = ignore all words with total frequency lower than this.
sizeRange = [10, 20, 40, 80, 160, 320] # size is the dimensionality of the feature vectors.
paramMinCount = 5 # window is the maximum distance between the current and predicted word within a sentence.

# end of names and parameters

####################################
# List of all possible parameters for Word2Vec:
# class gensim.models.word2vec.Word2Vec(sentences=None, size=100, alpha=0.025, window=5, min_count=5, max_vocab_size=None, sample=0.001, seed=1, workers=3, min_alpha=0.0001, sg=0, hs=0, negative=5, cbow_mean=1, hashfxn=<built-in function hash>, iter=5, null_word=0, trim_rule=None, sorted_vocab=1, batch_words=10000)
# Documentation: https://radimrehurek.com/gensim/models/word2vec.html#gensim.models.word2vec.Word2Vec
####################################

import gensim, os

class MySentences(object):
    # Following tutorial from by
    # http://rare-technologies.com/word2vec-tutorial/
    def __init__(self, dirname):
        self.dirname = dirname

    def __iter__(self):
        for fname in os.listdir(self.dirname):
            for line in open(os.path.join(self.dirname, fname)):
                yield line.split()

for paramWindow in windowRange:
    for paramSize in sizeRange:
        case = 'medline-with-abs-w'+str(paramWindow)+'-s'+str(paramSize)+'-mc'+str(paramMinCount)
        myModelName = modelDir+'/'+case+'-model.gensim'

        termsPath = modelDir+case+'-terms.txt'
        tensorPath = modelDir+case+'-tensor.txt'

        sentences = MySentences(datasetDir) # a memory-friendly iterator
        print "Generating model"
        model = gensim.models.Word2Vec(sentences, min_count=paramMinCount, size=paramSize, window=paramWindow)
        model.save(myModelName)
        print "Model saved"
        print

        #################
        # console operations:

        # load moel:
        #model = gensim.models.Word2Vec.load('../MyModels/medline-Clean-w5-s100-mc10-model')

        #################
        # Extract terms from the model:

        print "Extracting terms"
        terms = ''
        terms_per_line = ''
        for term in model.vocab:
            terms +=term+' | '
            #terms_per_line +=term+'\n'
        print "\t number of terms: "+str(len(terms.split(' | ')))

        f = open(termsPath, 'w')
        #f.write(terms_per_line)
        f.write(terms)
        f.close()
        print "Terms file saved as "+termsPath
        print

        ###################
        # Buils vector file
        print "Creating tensor"
        mytensor = ''
        for t in terms.split(' | '):
            mytensor += t+' '
            if len(t)>0:
                for line in model[t]:
                        mytensor += str(line)+' '
                mytensor += '\n'
        #In [22]:
        print "\t number of vectors in the tensor: "+str(len(mytensor.split('\n')))

        f = open(tensorPath, 'w')
        f.write(mytensor)
        f.close()
        print "Tensor saved as "+tensorPath
        print "... all done!"
        print
