# needed imports
from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage
import numpy as np
import random

for n in range(1,8):
	#orig=('/home/jaume/ACCUMULATE/data/pca2/pca2medscape.tok.sg.d50.w'+str(n)+'.txtuncommon')
	#x = np.loadtxt(orig, delimiter=', ', usecols=(0,1))

	orig=('/home/jaume/ACCUMULATE/data/tsne/tsne2medscape.tok.sg.d50.w'+str(n)+'.txt.npy')
	x = np.load(orig)
	print x
	# Linkage matrix (default method euclidean)
	z = linkage(x[100000:103000])

	# calculate full dendrogram
	plt.figure(figsize=(25, 10))
	plt.title('Hierarchical Clustering Dendrogram')
	plt.xlabel('First 3000 NON frequent terms (from tsne) for file '+str(n))
	plt.ylabel('distance')
	dendrogram(z,
		leaf_rotation=90.,  # rotates the x axis labels
		leaf_font_size=0.,  # font size for the x axis labels
	)
	plt.savefig('../data/charts/tsne-dendogram'+f+'_-uncommon.png')
	plt.show()
