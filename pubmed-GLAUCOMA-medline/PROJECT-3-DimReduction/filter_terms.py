import numpy as np
import mdp
import matplotlib.pyplot as plt

def check_lines(method, filenames, words):
	for fn in filenames: #no need for iteration since all files have same terms
		count = 0;count_no = 0
		common = '';uncommon = ''
		with open(fn, 'r') as data:
			for line in data:
				#print line.split(" ")[0]
				if line.split(" ")[0].strip() in words:
					#print line.split(" ")[0].strip()
					count+=1
					common+=line
				else:
					#print "#### "+line.split(" ")[0].strip()
					count_no+=1
					uncommon+=line
		print fn
		print "words that are in the dataset: "+str(count)+" ("+str(count*100/count_no)+"%)"
		print "words that are NOT in the dataset: "+str(count_no)+" ("+str(100-(count*100/count_no))+"%)"
		# write filtered terms in new files
		f_common = open(fn+'common', 'w+')
		f_common.write(common)
		f_common.close()
		f_uncommon = open(fn+'uncommon', 'w+')
		f_uncommon.write(uncommon)
		f_uncommon.close()
	return


def filter_by_term(filenames):
	import nltk
	from nltk.corpus import wordnet

	#########################
	## List of NLTK tagset() in 5 groups:
	# Adjective:
	g0 = ["JJ", "JJR", "JJS"]
	# OTHER:
	g1 = ["PRP", "PRP$", "WP", "WP$", "DT", "WDT","CC", "IN", "RB", "RBR", "RBS", "WRB"]
	# Nouns:
	g2 = ["NN", "NNS"]
	# Noun proper:
	g3 = ["NNP", "NNPS"]
	# Symbols, Numbers:
	g4 = ["$", "''", "(", ")", ",", "--", ".", ":", "CD", "EX", "FW", "LS", "PDT", "POS", "RP", "SYM", "TO", "UH", "``"]
	# Verbs:
	g5 = ["MD", "VBD", "VBG", "VBN", "VBP", "VBZ"]

	for fn in filenames: #no need for iteration since all files have same terms
		total = 1
		count_0 = 0;count_1 = 0;count_2 = 0;count_3 = 0;count_4 = 0;count_5 = 0;count_6 = 0;
		str_0 = '';str_1 = '';str_2 = '';str_3 = '';str_4 = '';str_5 = '';str_6 = ''
		with open(fn, 'r') as data:
			for line in data:
				total+=1
				token = line.split()[0].strip()
				token = token.strip('.').strip(',')
				#token = token.strip(',')
				#token = token.strip(';')
				#print token,
				# Check word kind:
				kind = nltk.pos_tag([token])[0][1]
				# switch:
				if kind in g0:
					count_0+=1
					str_0+=line
				elif kind in g1:
					count_1+=1
					str_1+=line
				elif kind in g2:
					if wordnet.synsets(token.decode('utf-8')):
						count_2+=1
						str_2+=line
					else:
						count_6+=1
						str_6+=line
						#print token+" | ",
				elif kind in g3:
					count_3+=1
					str_3+=line
				elif kind in g4:
					count_4+=1
					str_4+=line
				elif kind in g5:
					count_5+=1
					str_5+=line

		print fn
		print
		print "TOTAL terms = "+str(count_0+count_1+count_2+count_3+count_4+count_5)
		print "0_Adjective: "+str(count_0)+" ("+str(count_0*100/total)+"%)"
		print "1_    Other: "+str(count_1)+" ("+str(count_1*100/total)+"%)"
		print "2_    Nouns: "+str(count_2)+" ("+str(count_2*100/total)+"%)"
		print "3_ Proper N: "+str(count_3)+" ("+str(count_3*100/total)+"%)"
		print "4_  symbols: "+str(count_4)+" ("+str(count_4*100/total)+"%)"
		print "5_    Verbs: "+str(count_5)+" ("+str(count_5*100/total)+"%)"
		print "5_    Noise: "+str(count_6)+" ("+str(count_6*100/total)+"%)"

		for n in range(1,6):
			# write filtered terms in new files
			name0 = 'token-kind_'
			f = open(fn+name0+str(n), 'w+')
			f.write(eval('str_'+str(n)))
			f.close()

	return


# Loading 5000 most frequent Engl;ish words from http://www.wordfrequency.info/top5000.asp
words = []
words_list = open('../data/external-data/5000words.txt').readlines()
for w in words_list:
	words.append(w.replace('\n',''))

# filenames list of the seven dataset files
filenames = []
path = "../data/"
fn0 = "medscape.tok.sg.d50.w"
for n in range(1,8):
	filenames.append(path+fn0+str(n)+".txt")

#check_lines("count", filenames, words)

#check_lines("filter", filenames, words)


filenames2 = []
path2 = "../data/tsne/"
fn0 = "medscape.tok.sg.d50.w"
for n in range(1,8):
	filenames2.append(path2+fn0+str(n)+".txt")

#filter_by_term(filenames2)

filenames3 = []
path3 = "../data/"
fn0 = "medscape.tok.sg.d50.w"
for n in range(1,2):
	filenames3.append(path3+fn0+str(n)+".txt")

filter_by_term(filenames3)
