
# -*- coding: iso-8859-15 -*-

def getline(filename, lookup, sim):
	with open(filename) as myFile:
	   	for num, line in enumerate(myFile, 1):
			for l in lookup:
				if sim == "eq":
					if l == line.strip():
						print "'"+line.strip()+"': "+str(num-1)+", ",
				if sim == "present":
					if l in line:
						print "'"+line.strip()+"': "+str(num-1)+", ",

	print "Done "+sim
filename = "list_terms.txt"

body_parts = {'arm', 'eye', 'eyebrow', 'belly', 'leg', 'breast', 'thumb', 'elbow', 'fist', 'finger', 'foot (plural: feet)', 'ankle', 'buttocks', 'hair', 'neck', 'hand', 'wrist', 'hip', 'chin', 'knee', 'head', 'lip', 'mouth', 'nose', 'nostril', 'upper arm', 'thigh', 'ear', 'bottom, bum', 'back', 'underarm, forearm', 'lower leg', 'shoulder', 'forehead', 'waist', 'calf (plural: calves)', 'cheek', 'eyelash, lash', 'tooth (plural: teeth)', 'toe', 'tongue'}

health_jobs = {"Doctor", "Assistant​", 'nurse​','Practical', 'Physician', 'Therapist', 'Pharmacy', 'Technician​', 'Sonographer​', 'Clinical', 'Dental', 'Pharmacist​', 'Radiolog', 'Hygienist​', 'Occupational', 'Speech', 'Pathologist​', 'Respiratory', 'Practitioner​', 'GP', 'Phlebotomist​', 'Surgical', 'Veterinar', 'Psychiatric', 'Transcriptionist​', 'Specialist​', 'Caretaker​', 'Massagi', 'Optician​', 'Dietician​', 'Orderly​', 'Surgeon​', 'Anesthetist​', 'Pediatrician​', 'Anesthesiologist​', 'Optometrist​', 'Chiropractor​', 'Psychiatrist​', 'Obstetrician​'}



# A group of proper names + entity recognition, and then grouping by entity type (location, person, ...)

#lookup = {"anuary","ebruary","March\n", "June", "July", "August","Setember", "October", "noovember", "December"}
#lookup = {'onday', 'uesday', 'ednesday', 'hrusday', 'riday', 'aturday', 'unday'}
#lookup = {"Doctor",}
#lookup = {'the'}
#lookup = health_jobs
lookup = body_parts

sim = "eq"

getline(filename, lookup, sim)

print
print "######################"
print

sim = "present"

getline(filename, lookup, sim)

