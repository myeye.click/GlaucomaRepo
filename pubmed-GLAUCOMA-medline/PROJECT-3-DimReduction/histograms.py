def do_hist_muiltidim(f1, f2):
	r = range(1,51) ## 50 simensions
	## get the tensor
	new1 = []
	val1 = np.loadtxt('../data/'+f1, delimiter=' ', usecols=(r))
	for v in val1:
		## Find length for each vector of the tensor 
		new1.append(np.linalg.norm(v))
	## get the tensor
	new2 = []
	val2 = np.loadtxt('../data/'+f2, delimiter=' ', usecols=(r))
	for v in val2:
		## Find length for each vector of the tensor 
		new2.append(np.linalg.norm(v))	

	## Sustraction 	
	values = np.subtract(new2, new1)
	
	plt.hist(values, bins=100, histtype='stepfilled', normed=True, color='b')
	plt.title("features increment")
	plt.xlabel(f2+" - "+f1)
	plt.ylabel("Count")
	plt.legend()
	#plt.show()
	plt.savefig('../data/histograms/'+f1+"---"+f2+'.png', transparent=True)



############################################################################
############################################################################

filenames = []
fn0 = "medscape.tok.sg.d50.w"
for n in range(1,8):
	filenames.append(fn0+str(n)+".txt")

for i in range(0,7):
	for j in range(0,7):
		if i != j:
			print "processing "+str(i)+"<-->"+str(j)
			do_hist_muiltidim(filenames[i], filenames[j])

