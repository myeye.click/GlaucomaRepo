#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Command line tool for VDAtersors app
"""

#from pca import *
#import term_groups
#import sys

#from os import walk

## main class 
class main():
	def __init__(self):
		self.path = "../data/"
		self.welcomeText()
	## my console
	def console(self):
		keep_going = True
		count = 0
		while keep_going:
			command = raw_input('VDAtensors ['+str(count)+']: ').strip()
			if command == '0':
				self.tensorIntegrity()
				count += 1
			elif command == '1':
				self.dimensionReduction()
				count += 1
			elif command == '2':
				self.plot(kindOf)
				count += 1
			elif command == 'h':
				self.printHelp()
				count += 1
			elif command == 'q':
				print ('bye!')
				print 
				keep_going = False
			else:
				print('Invalid Command. ("h" for help)')
				count += 1

	def list_files(self):
		# List files in self.path
		from os import listdir
		from os.path import isfile, join
		for f in [ff for ff in listdir(self.path) if isfile(join(self.path, ff))]:
			print f
		return


	def tensorIntegrity(self):
		'''check the integrity of tensor file(s) and report it '''
		from pca import check_lines
		#list files:
		self.list_files()
		#Choose a file to check and call checker
		tensor = raw_input('Which file you want to check? ').strip()
		check_lines(self.path+tensor)
		return

	def dimensionReduction(self):
		#list files
		self.list_files()
		#Choose file or files from the list
		data = raw_input('Choose 1 or more files (separated by commas) ').strip()
		filenames = data.split()
		dimensions = raw_input('Choose a number of dimensions to redice the tensor(s) to: ').strip()
		format = raw_input('Choose a format for the new tensor file(s) (1) npy, (2) txt: ').strip()
		#Choose dimemsion reduction method
		print 'Which dimension reduction method you want to apply?'
		print '\t(1)  pca\n\t(2) t-SNE\n\t(b) back'
		dialog = True
		methods = {1:"pca",2:"t-sne"}
		while dialog == True:
			method = raw_input('Choose method: ').strip()
			if method=="b" :
				dialog = False
			elif method=="1":
				#Go ahed with the method
				
				#################################################
				from pca import do_pca
				for f in filenames:
				  do_pca(self.path, f, format, dimensions)
								
				#if method
				
				
				dialog = False
			
		return


	def welcomeText(self):
		## Welcome text:
		print
		print "Welcome to VDAtensors."
		print "This is a bunch of python scripts applied to tersors."
		print "A tensor is defined as a number of vectors (rows) with a number of features (or dimensions) each (columns). E.g.:"
		print "A tensor of M vectors with N features each."
		print 
		print "\tvector1name feature1-1 feature1-2 feature1-3 ...  feature1-N"
		print "\tvector2name feature2-1 feature2-2 feature2-3 ...  feature2-N"
		print "\tvector3name feature3-1 feature3-2 feature3-3 ...  feature3-N"
		print "\t... . . . . . . . . . . . . . . . . .  ...  ...  ..."
		print "\tvectorMname featureM-1 featureM-2 featureM-3 ...  featureM-N"
		print
		print "(*) vector*name are strings. Feature* are numbers"
		print
		return

	def printHelp(self):
		print
		print ('Available groups of operations are: 1 or 2')
		print ('\t(1) Dimension reduction (pca and t-SNE)')
		print ('\t(2) Plotting (one tensor, two tensors)')
		print 
		return

	def filenames(path):
		'''
		  Create a list og files to work with
		'''
		filenames = []
		fn0 = path+"medscape.tok.sg.d50.w"
		for n in range(1,8):
			filenames.append(fn0+str(n)+".txt")
		return filenames


if __name__ == "__main__":
    #main()
	main = main()
	main.console()




#############################################
#### NOTES:


## check permissions to write in data directory



# you can also provide a second argument: the number of vectors you wish to display
#if len(sys.argv) != 4:
#	print "Usage: python main.py ARG1 ARG2 ARG3\n\tARG1: \n\tARG2: \n\tARG3:"
#else:
#	print sys.argv
	
	''' 
	OPTIONS:
	pca.py:
		ARG1 = do_pca(filenames, format, dimensions, num_features) -> Calculate pca for a list of files
			filenames: list of filenames
			format: "npy" for npy file. Anything else for txt file
			dimensions: number of dimensions to reduce to
			num_features: number of features of each vestor to be considered
			
'''

