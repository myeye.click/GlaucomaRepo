import numpy as np
import pandas as pd



def do_TSNE(filenames, format):
	#from sklearn.manifold import TSNE

	from tsne import bh_sne
	
	terms = 50000
	dim = 2 # Set num of dimensions to reduce to	
	#model = TSNE(n_components=dim, random_state=0)
	#np.set_printoptions(suppress=True)
	r = range(1,51)	
	colors=['#FCC9D1','#E2A8AF','#C8888E','#AE676D','#94474B','#7A262A','#600609']
	for f in filenames:
		#values = np.loadtxt('../data/filtered/'+f+'uncommon', delimiter=' ', usecols=(r))
		#values = np.loadtxt('../data/filtered/'+f+'uncommon', delimiter=' ', usecols=(r))
		#my_tsne = model.fit_transform(values[:10000]) 

		values = np.loadtxt('../data/'+f, delimiter=' ', usecols=(r))
		#my_tsne = model.fit_transform(values) 
		#my_tsne = model.fit_transform(values[:terms])
		my_tsne = bh_sne(values)
		
		if format == "npy":
			np.save('../data/tsne/tsne'+str(dim)+f, my_tsne)
		else:
			tsne_str = str(my_tsne.tolist()).replace("], [", "\n").replace("[[", "").replace("]]", "")
			new = open('../data/tsne/tsne-ALL-'+str(dim)+f+'.txt', 'w+')
			new.write(str(tsne_str))
			new.close()
		print f

	return



filenames = []
fn0 = "medscape.tok.sg.d50.w"
for n in range(1,8):
	filenames.append(fn0+str(n)+".txt")

do_TSNE(filenames, "npyXX")

