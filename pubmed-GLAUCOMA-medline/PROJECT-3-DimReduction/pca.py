import numpy as np
import mdp
import matplotlib.pyplot as plt
import pandas as pd

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

'''
notes:
if a is an nparray, then a[:,0] are the 1D array of the first column

r = range(1,50)
values = np.loadtxt('element2.txt', delimiter=' ', usecols=(r))
labels = np.loadtxt('file.txt', delimiter=' ', dtype=np.str, usecols=[0])

Due to errors of mdp processing lines that contain "#" , as a straight dirty solution IO replave "#" with "&#35" using perl:
perl -i.bak  -p -e 's/#/&#35/g;' medscape*.txt
'''

def check_lines(filename):
	c=0;inc=0;out = ''
	with open(filename) as data:
		num_columns = len(data.readlines()[0].split(" "))
		num_rows = sum(1 for _ in data)
	with open(filename) as data:
		for line in data:
			c+=1
			#print "####"+line
			if len(line.split(" ")) != num_columns:
				out += "-> line "+str(c)+") "+line
				inc+=1
		# report:
		print
		print "----> Report:"
		print "The tensor ("+filename+") has "+str(c)+" vectors and "+str(num_columns)+" columns:"
		if out != "":
			print "Inconsistent lines found: "+str(inc)
			print out
		else:
			print "None inconsistent lines found"
	return

#check_lines("../data/lala")

def do_pca2(path, path2, filename, format, dimensions):
	import re
	a=''
	with open(path+filename) as data:
		num_columns = len(data.readlines()[0].strip().split(" "))
		num_rows = sum(1 for line in open(path+filename))
		print " - num_columns --> "+str(num_columns)
		print " - num rows --> "+str(num_rows)

	r = range(1, num_columns)
	c = 1
	# Directory name:
	import os
	#new_dir = time.strftime("%Y-%m-%d-%H-%M")+"pca-"+str(dimensions)
	if num_columns > 4:
		values = np.loadtxt(path+filename, usecols=(r))
		##print "values: "+str(values.item(0,0)) #+" | "+str(values.item(values.shape))
		print "... shape values: "+str(values.shape)
		pca = mdp.pca(values, reduce=True, output_dim=int(dimensions))
		print "... shape pca: "+str(pca.shape)
		##print(pca)

		# Check and create directory:
		new = path2+'/pca'+str(dimensions)+'-'+filename
		if not os.path.exists(os.path.dirname(new)):
			##print "HEYYYYYYYYYYYYYY!!"
			try:
				os.makedirs(os.path.dirname(new))
			except OSError as exc: # Guard against race condition
				if exc.errno != errno.EEXIST:
					raise

		if format == "npy":
			np.save(new, pca)
		else:
			pca_str = str(pca.tolist()).replace("], [", "\n").replace("[[", "").replace("]]", "")
			##print pca_str
			#pca_str = str(pca.tolist())
			print " - terms list length: "+str(len(pca))

			final = open(new, 'w+')
			final.write(str(pca_str))
			final.close()

	print
	return


def do_pca(path, path2, filename, format, dimensions):
	import re
	a=''
	with open(path+filename) as data:
		print
		num_columns = len(data.readlines()[0].strip().split(" "))
		num_rows = sum(1 for line in open(path+filename))
		print "num_columns --> "+str(num_columns)
		print "num rows --> "+str(num_rows)

	r = range(1, num_columns)
	c = 1
	#colors=['#FCC9D1','#E2A8AF','#C8888E','#AE676D','#94474B','#7A262A','#600609']
	# Directory name:
	import time, os
	#new_dir = time.strftime("%Y-%m-%d-%H-%M")+"pca-"+str(dimensions)
	if num_columns > 4:
		values = np.loadtxt(path+filename, usecols=(r))
		pca = mdp.pca(values, reduce=True, output_dim=int(dimensions))
		# Check and create directory:
		#new = path2+new_dir+'/pca'+str(dimensions)+'-'+filename
		new = path2+'/pca'+str(dimensions)+'-'+filename
		if not os.path.exists(os.path.dirname(new)):
			try:
				os.makedirs(os.path.dirname(new))
			except OSError as exc: # Guard against race condition
				if exc.errno != errno.EEXIST:
					raise

		if format == "npy":
			np.save(new, pca)
		else:
			pca_str = str(pca.tolist()).replace("], [", "\n").replace("[[", "").replace("]]", "")
			print ">> terms list length"+str(len(pca))
    		#tmp = ''; l = 0
    		#terms = np.genfromtxt('../data/'+f, delimiter=' ', usecols=(0,1))
    		#terms = pd.read_csv('../data/'+f, delimiter=' ', usecols=[0])
    		#print terms[:10]
    		#for line in pca_str.split('\n') :
    		#	tmp += terms[l]+" "+line
    		#	l+=1
			final = open(new, 'w+')
			final.write(str(pca_str))
			final.close()

	return

def do_plot_by_term(filenames, terms):
	values = np.zeros(shape=(1,2))
	#colors=['#FCC9D1','#E2A8AF','#C8888E','#AE676D','#94474B','#7A262A','#600609']
	#colors=['#FCC9D1','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#600609']
	colors=['#FD0202','#EDB564','#39F11F','#0B6EFF','#A711E6','#B2ADB0','#000000']
	#terms = 3800
	#z = range(1,len(filenames)+1)

	for f in filenames:
		val = np.loadtxt('../data/pca3/pca'+f, delimiter=', ', usecols=(0,1))
		for i in range(0,terms):
			values = np.vstack((values, val[i]))
			print '.',
		print i
		values = values[1:] ## removing firt temp row

		#plt.scatter(values[:,0], values[:,1], c=int(values.size), s=100, cmap='gray_r')
		plt.scatter(values[:,0], values[:,1], s=20, color=colors, alpha=0.005)

		plt.xlabel('7 files (pca=2), 3800 common terms')
		plt.show()
	return

def do_3d_plot(filenames, terms):
	from mpl_toolkits.mplot3d import Axes3D

	fig = plt.figure()
	ax = fig.gca(projection='3d')

	x = np.linspace(0, 1, 100)
	y = np.sin(x * 2 * np.pi) / 2 + 0.5
	ax.plot(x, y, zs=0, zdir='z', label='zs=0, zdir=z')

	colors = ('r', 'g', 'b', 'k')
	for c in colors:
		for f in filenames:
			val = np.loadtxt('../data/pca3/pca'+f, delimiter=', ', usecols=(0,1,2))
			print f
			for i in range(0,terms):
				v = val[i]
				print v
				x = v[0]
				y = v[1]
				z = v[2]

				#x = np.random.sample(20)
				#y = np.random.sample(20)
				ax.scatter(x, y, z, zdir='y', c=c)

			ax.legend()
			ax.set_xlim3d(0, 1)
			ax.set_ylim3d(0, 1)
			ax.set_zlim3d(0, 1)

			plt.show()
	return


def do_TSNE(path, path_tsne, f, format):
	from sklearn.manifold import TSNE
	print ". . . hello from do_TSNE "+path+f
	with open(path+f) as data:
		num_columns = len(data.readlines()[0].split(" "))
		num_rows = sum(1 for _ in data)

	terms = 13287
	dim = 2 # Set num of dimensions to reduce to
	model = TSNE(n_components=dim, random_state=0)
	np.set_printoptions(suppress=True)
	r = range(1,num_columns-1)
	#colors=['#FCC9D1','#E2A8AF','#C8888E'		,'#AE676D','#94474B','#7A262A','#600609']
	#values = np.loadtxt('../data/filtered/'+f+'uncommon', delimiter=' ', usecols=(r))
	#my_tsne = model.fit_transform(values[:10000])
	#if num_columns > 4 and "mc30" in f:
	print "processing file: "+f
	values = np.loadtxt(path+f, usecols=(r))
	#my_tsne = model.fit_transform(values)
	my_tsne = model.fit_transform(values[:terms])

	if format == "npy":
		np.save(path_tsne+'/tsne-'+str(num_rows)+'terms-'+f, my_tsne)
	else:
		tsne_str = str(my_tsne.tolist()).replace("], [", "\n").replace("[[", "").replace("]]", "")
		new = open(path_tsne+'tsne-'+str(num_rows)+'terms-'+f+'.txt', 'w+')
		new.write(str(tsne_str))
		new.close()
	print f
	'''
	plt.scatter(my_tsne[:,0], my_tsne[:,1], alpha=0.1) ##, color='#FFD159')
	#plt.plot(pca[:100])
	#plt.xlabel(f+"file: "+f+" (my_tsne 2)")


	values = np.loadtxt('../data/filtered/'+f+'common', delimiter=' ', usecols=(r))
	my_tsne = model.fit_transform(values)

	if format == "npy":
		np.save('../data/tsne/tsne'+str(dim)+f+'common', my_tsne)
	else:
		pca_str = str(pca.tolist()).replace("], [", "\n").replace("[[", "").replace("]]", "")
		new = open('../data/pca'+str(dim)+'/pca'+str(dim)+f, 'w+')
		#new.write(str(pca_str))
		new.close()


	plt.scatter(my_tsne[:,0], my_tsne[:,1], color="#FCC9D1", alpha=0.5)
	#plt.plot(my_tsne[:100])

	plt.xlabel("file: "+f+" (red common terms | blue uncommon terms )")
	#plt.xlabel("alpha 7 files ONLY common terms )")
	plt.savefig('../data/charts/tsne'+f+'_common-uncommon.png')
	plt.show()
	'''
	return

#medscape.tok.sg.d50.w7.txttoken-kind_5
def do_plot_by_token_kind(filenames):
	values = np.zeros(shape=(1,2))
	#colors=['#FCC9D1','#E2A8AF','#C8888E','#AE676D','#94474B','#7A262A','#600609']
	#colors=['#FCC9D1','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#600609']
	colors=['#FD0202','#EDB564','#39F11F','#0B6EFF','#A711E6','#B2ADB0','#000000']
	terms = 3800
	#z = range(1,len(filenames)+1)

	for f in filenames:
		for i in range(1,6):
			val = np.loadtxt('../data/token-kind/'+f+'token-kind_'+str(i), delimiter=' ', usecols=(0,1))
			#print values, val[0]

			values = np.vstack((values, val[i]))
			print '.',
			#print f
		print i
		values = values[1:] ## removing firt temp row

		#plt.plot(values[:,0], values[:,1])
		#plt.plot(range(0,len(values[:,0])), values[:,0])

		plt.scatter(values[:,0], values[:,1], s=20, color=colors, alpha=0.005)

	plt.xlabel('...')
	plt.show()
	return

def do_arrow_plot(comp_files, follow, terms_label):
	# set vars:
	#follow=my_months
	#terms_label= "MONTHS"
	# number of terms to draw
	t = 30000
	# colors
	colors = ['y','c','k','g','m','b','r']
	# axes length
	plt.axis([-5,8,-8,5])

	values = []
	# Get point from files
	for f in comp_files:
		values.append(np.loadtxt('../data/pca2/pca'+f, delimiter=', ', usecols=(0,1)))
		#values.append(np.loadtxt('../data/tsne-all/tsne-ALL-2'+f+'.txt', delimiter=', ', usecols=(0,1)))
		#values.append(np.load('../data/tsne/tsne2'+f+'.npy'))
	mycolor = ''
	for v in range(0,t):
		for n in range(0,len(values)-1):
			if n==0:
				plt.arrow(values[0][v][0], values[0][v][1], values[1][v][0], values[1][v][1], head_width=0.1, head_length=0.1, fc=colors[n+1], ec=colors[n])
				n+=1
			else:
				plt.arrow(float(values[n-1][v][0])+float(values[n][v][0]), float(values[n-1][v][1])+float(values[n][v][1]), float(values[n][v][0])+float(values[n+1][v][0]), float(values[n][v][1])+float(values[n+1][v][1]), head_width=0.1, head_length=0.1, fc=colors[n], ec=colors[n])
			if mycolor != str(colors[n]):
				print "THE COLOR ALL-TERMS: "+mycolor
				mycolor = str(colors[n])

	print "... all-terms arrays loaded"

	v = ''
	mycolor = ''
	for d in follow.items():
		v = d[1]
		for n in range(0,len(values)-1):
			if n==0:
				plt.arrow(values[0][v][0], values[0][v][1], values[1][v][0], values[1][v][1], head_width=0.1, head_length=0.1, fc=colors[::-1][n], ec=colors[::-1][n])
				n+=1
			else:
				plt.arrow(float(values[n-1][v][0])+float(values[n][v][0]), float(values[n-1][v][1])+float(values[n][v][1]), float(values[n][v][0])+float(values[n+1][v][0]), float(values[n][v][1])+float(values[n+1][v][1]), head_width=0.1, head_length=0.1, fc=colors[::-1][n], ec=colors[::-1][n])
		if mycolor != str(colors[n]):
			mycolor = str(colors[n])
			print "THE COLOR GROUP: "+mycolor

	print "... Group-terms arrays loaded"

	plt.grid(True, which='both')
	plt.axhline(y=0, color='k')
	plt.axvline(x=0, color='k')
	#ylabel = "States:"+str(comp)+" | Special terms(kind): "+terms_label+" | Common terms (num)="+str(t)
	ylabel = "States-"+'-'.join(map(str, comp))+"--Group-"+terms_label+"--Tnum-"+str(t)
	plt.xlabel(ylabel)
	plt.savefig('../data/img-webNEW/'+ylabel+'.png', transparent=True)
	#plt.show()

	print ylabel+" ... saved!"
	print "####################################################"
	return

def do_arrow_star_plot(comp_files, follow, terms_label):
	# set vars:
	#follow=my_months
	#terms_label= "MONTHS"
	# number of terms to draw
	t = 30000
	# colors
	colors = ['y','c','k','g','m','b','r']
	# axes length
	plt.axis([-5,8,-8,5])

	values = []
	# Get point from files
	for f in comp_files:
		values.append(np.loadtxt('../data/pca2/pca'+f, delimiter=', ', usecols=(0,1)))
		#values.append(np.loadtxt('../data/tsne-all/tsne-ALL-2'+f+'.txt', delimiter=', ', usecols=(0,1)))
		#values.append(np.load('../data/tsne/tsne2'+f+'.npy'))
	mycolor = ''
	for v in range(0,t):
		for n in range(0,len(values)-1):
			if n==0:
				plt.arrow(0, 0, values[1][v][0] - values[0][v][0], values[1][v][1] - values[0][v][1], head_width=0.1, head_length=0.1, fc=colors[n+1], ec=colors[n])
				n+=1
			else:
				plt.arrow(0, 0, float(values[n][v][0])+float(values[n+1][v][0]) - float(values[n-1][v][0])+float(values[n][v][0]), float(values[n][v][1])+float(values[n+1][v][1]) - float(values[n-1][v][1])+float(values[n][v][1]), head_width=0.1, head_length=0.1, fc=colors[n], ec=colors[n])
			if mycolor != str(colors[n]):
				print "THE COLOR ALL-TERMS: "+mycolor
				mycolor = str(colors[n])

	print "... all-terms arrays loaded"

	v = ''
	mycolor = ''
	for d in follow.items():
		v = d[1]
		for n in range(0,len(values)-1):
			if n==0:
				plt.arrow(0, 0, values[1][v][0] - values[0][v][0], values[1][v][1] - values[0][v][1], head_width=0.1, head_length=0.1, fc=colors[::-1][n], ec=colors[::-1][n])
				n+=1
			else:
				plt.arrow(0, 0, float(values[n][v][0])+float(values[n+1][v][0]) - float(values[n-1][v][0])+float(values[n][v][0]), float(values[n][v][1])+float(values[n+1][v][1]) - float(values[n-1][v][1])+float(values[n][v][1]), head_width=0.1, head_length=0.1, fc=colors[::-1][n], ec=colors[::-1][n])
		if mycolor != str(colors[n]):
			mycolor = str(colors[n])
			print "THE COLOR GROUP: "+mycolor

	print "... Group-terms arrays loaded"

	plt.grid(True, which='both')
	plt.axhline(y=0, color='k')
	plt.axvline(x=0, color='k')
	#ylabel = "States:"+str(comp)+" | Special terms(kind): "+terms_label+" | Common terms (num)="+str(t)
	ylabel = "States-"+'-'.join(map(str, comp))+"--Group-"+terms_label+"--Tnum-"+str(t)
	plt.xlabel(ylabel)
	plt.savefig('../data/arrow-star/'+ylabel+'.png', transparent=True)
	#plt.show()

	print ylabel+" ... saved!"
	print "####################################################"
	return

def do_hist_pca1(f1, f2):
	## Get numbers
	#values = np.zeros(shape=(1,2))
	val1 = np.loadtxt('../data/pca1/pca1'+f1, delimiter=' ')
	val2 = np.loadtxt('../data/pca1/pca1'+f2, delimiter=' ')

	values = np.subtract(val2, val1)

	plt.hist(values, bins=100, histtype='stepfilled', normed=True, color='b', label='Tensor inclrement')
	plt.title("features increment")
	plt.xlabel(f2+" = "+f1)
	plt.ylabel("Count")
	plt.legend()
	plt.show()

'''
def do_hist_muiltidim(f1, f2):
	r = range(1,51)
	new1 = []
	val1 = np.loadtxt('../data/'+f1, delimiter=' ', usecols=(r))
	for v in val1:
		new1.append(np.linalg.norm(v))

	new2 = []
	val2 = np.loadtxt('../data/'+f2, delimiter=' ', usecols=(r))
	for v in val2:
		new2.append(np.linalg.norm(v))

	values = np.subtract(new2, new1)

	plt.hist(values, bins=100, histtype='stepfilled', normed=True, color='b')
	plt.title("features increment")
	plt.xlabel(f2+" - "+f1)
	plt.ylabel("Count")
	plt.legend(DATASET-5-tsne.savefig('../data/histograms/'+f1+"---"+f2+'.png', transpaSET-6)
'''

############################################################################
############################################################################
############################################################################
############################################################################


path = '../DATASET-4-word2vec/'

path_in_tsne = '../DATASET-4-word2vec-filtered/'
path_tsne = '../DATASET-5-tsne/'
path2 = '../DATASET-6-PCA2/'
format = 'txt'
dimensions = 2

from os import walk
tmp = []
files = []
for (dirpath, dirnames, filenames) in walk(path_in_tsne):
    tmp.extend(filenames)
    break

for f in tmp:
    #print f, f[-10:]
    if f[-10:] == "tensor.txt":
        print ">>> "+f
        files.append(f)

for f in files:
    print "~~~~~~~~"
    print "processing file: "+f
    #do_pca2(path, path2, f, format, dimensions)
    do_TSNE(path, path_tsne, f, format)



###################################

#filenamescommon = [i + "common" for i in filenames]
#do_pca(filenames, "npyXX")

#do_plot_by_term(filenames, "lala")

#do_TSNE(filenames, "npyXX")

#do_plot_by_token_kind(filenames)


#####################################################################
## Groups of terms:


'''
fn0 = "medscape.tok.sg.d50.w"
# Loop for files:
for f1 in range(6,7):
	for f2 in range(4,5):
		comp = []
		comp_files = []
		#if f1 == 7:
		#	comp = [1, 7]
		#else:
		if f1 > f2:
			comp = [f1, f2]
		if len(comp)>0:
			#print comp
			for n in comp:
				comp_files.append(fn0+str(n)+".txt")
			#loop for groups of terms:
			for g in groups:
				print " ... working -> ",
				print comp, comp_files
				do_arrow_plot(comp_files, g, glabels[groups.index(g)])

'''


# Interesting groups of words to follow:
my_days = {'Friday': 77807,  'Saturday': 88049,  'Monday': 107364,  'Sunday': 118883,  '<newparagraph>Munday': 129548}

my_months = {'June': 2966,  'March': 3227,  'October': 3267,  'December': 3294,  'January': 3409,  'July': 3704,  'February': 3894,  'August': 3917, 'June.': 47809,  'October.': 49996,  '<newparagraph>March': 51816,  'July.': 53436, '5555-June': 65255, 'February.': 71605,  'September-October': 71751, 'December.': 75879,  'Auguste': 79177,  'January.': 80506,  'May-June': 87982,  '<newparagraph>Augustin': 89141,  '5555-March': 90488,  'Augusto': 92753,  '5555-January': 94730,  'August.': 104009,  'August-September': 107869,  'August-October': 108919,  'August-October': 108919,  'Augustini': 111443,  'June-July': 112725,  'June-July': 112725,  '55-June': 115108,  'mid-August': 125273,  '<newparagraph>Augusto': 126527,  '.<newparagraph>March': 127702,  '<newparagraph>Juneja': 135138}

my_monthsClean = {'June': 2966,  'March': 3227,  'October': 3267,  'December': 3294,  'January': 3409,  'July': 3704,  'February': 3894,  'August': 3917, 'June.': 47809,  'October.': 49996,  '<newparagraph>March': 51816,  'July.': 53436, '5555-June': 65255, 'February.': 71605,  'September-October': 71751, 'December.': 75879,  'Auguste': 79177,  'January.': 80506,  'May-June': 87982, '5555-March': 90488, '5555-January': 94730,  'August.': 104009,  'August-September': 107869,  'August-October': 108919,  'August-October': 108919, 'June-July': 112725,  'June-July': 112725,  '55-June': 115108, 'mid-August': 125273, '.<newparagraph>March': 127702}

my_bodyparts = {'head': 519,  'neck': 531,  'breast': 705,  'eye': 1183,  'hand': 1218,  'knee': 1228,  'ear': 1242,  'hip': 1318,  'back': 1378,  'hair': 1498,  'shoulder': 1509,  'tongue': 1848,  'lip': 1978,  'wrist': 2039,  'elbow': 2171,  'nose': 2217,  'arm': 2248,  'mouth': 2279,  'ankle': 2371,  'leg': 2933,  'thumb': 3029,  'finger': 3102,  'forehead': 3706,  'thigh': 5303,  'toe': 5785,  'cheek': 6754,  'chin': 7565,  'belly': 9741,  'eyebrow': 10967,  'buttocks': 13680,  'nostril': 14744,  'waist': 17552,  'fist': 28296}

my_jobs = {'Physicians': 239, 'Physician': 711, 'PhysiciansDisclosure': 3046, 'Radiologists': 3349, 'GP': 3819,  'Physicians-American': 4279,  'Radiologist': 4332,  'Occupational': 4586, 'Practical': 8573,  'Physicians<newparagraph>Disclosure': 8843,  'GP.': 9234, 'Physicians.': 11698, 'RadiologistsDisclosure': 16996, 'Doctors': 24000, 'Doctor': 28449,  'Physician-in-Chief': 28457, '<newparagraph>Occupational': 32055,  '<newparagraph>Physicians': 35433, 'PhysiciansDaniel': 41151, 'PharmacyDisclosure': 42670, 'PhysiciansNeil': 46424,  'Radiologists<newparagraph>Disclosure': 46690, '<newparagraph>Psychiatric': 50295, 'Physician-In-Charge': 55936, 'PhysicianJ': 62872, 'PhysiciansRobin': 64618, 'Radiologist-in-Chief': 65716, 'Physician<newparagraph>Kilbourn': 69547,  'SurgicalDavid': 70174, 'RadiologistsUdo': 72610, '<newparagraph>Doctor': 74374, 'PhysicianKilbourn': 75970,  'Neuro-Radiologist': 87642,  'patients.<newparagraph>Clinical': 88910,  '<newparagraph>Physician': 89069,  'Veterinarians': 93932,  'Therapists': 94445, 'PhysiciansGlen': 110887, 'Radiologist.': 117604,  'RadiologistsDjamil': 118864, 'Physician/Neurologist': 127214, 'patients.<newparagraph>Surgical': 130852, '<newparagraph>Radiologists': 136049, 'Veterinarians.': 143988}

#my = {'the': 3}

groups = [my_days, my_months, my_monthsClean, my_bodyparts, my_jobs]

glabels = ["days", "months_wide", "months", "bodyparts", "jobs"]

'''
fn0 = "medscape.tok.sg.d50.w"
# Loop for files:
for f1 in range(6,7):
	for f2 in range(4,5):
		comp = []
		comp_files = []
		#if f1 == 7:
		#	comp = [1, 7]
		#else:
		if f1 > f2:
			comp = [f1, f2]
		if len(comp)>0:
			#print comp
			for n in comp:
				comp_files.append(fn0+str(n)+".txt")
			#loop for groups of terms:
			for g in groups:
				print " ... working -> ",
				print comp, comp_files
				do_arrow_star_plot(comp_files, g, glabels[groups.index(g)])
'''
