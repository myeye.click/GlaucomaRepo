import numpy as np
import matplotlib
import matplotlib.pyplot as plt

comp_files = []
comp = [1,2]
fn0 = "medscape.tok.sg.d50.w"
for n in comp:
	comp_files.append(fn0+str(n)+".txt")

#Make a random array and then make it positive-definite
#num_vars = 6
#num_obs = 9
#A = np.random.randn(num_obs, num_vars)

#values = []
# Get point from files
#for f in comp_files:	
A = np.loadtxt('../data/'+fn0+'7.txt', delimiter=' ', usecols=range(1,51))
#values.append(np.loadtxt('../data/tsne-all/tsne-ALL-2'+f+'.txt', delimiter=', ', usecols=(0,1)))
#values.append(np.load('../data/tsne/tsne2'+f+'.npy'))


A = np.asmatrix(A.T) * np.asmatrix(A)
U, S, V = np.linalg.svd(A) 
eigvals = S**2 / np.cumsum(S)[-1]

fig = plt.figure(figsize=(8,5))
sing_vals = np.arange(50) + 1
plt.plot(sing_vals, eigvals, 'ro-', linewidth=2)
plt.title('Scree Plot')
plt.xlabel('Principal Component')
plt.ylabel('Eigenvalue')
#I don't like the default legend so I typically make mine like below, e.g.
#with smaller fonts and a bit transparent so I do not cover up data, and make
#it moveable by the viewer in case upper-right is a bad place for it 
leg = plt.legend(['Eigenvalues from SVD'], loc='best', borderpad=0.3, 
                 shadow=False, prop=matplotlib.font_manager.FontProperties(size='small'),
                 markerscale=0.4)
leg.get_frame().set_alpha(0.4)
leg.draggable(state=True)
plt.show()
