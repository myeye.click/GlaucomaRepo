
def mygroups():

  '''
  TOTAL terms = 55811
  0_Adjective: 11604 (7%)
  1_    Other: 2374 (1%)
  2_    Nouns: 28780 (19%)
  3_ Proper N: 6671 (4%)
  4_  symbols: 507 (0%)
  5_    Verbs: 5875 (3%)
  5_    Noise: 91291 (61%)
  '''

  # Interesting groups of words to follow:
  my_days = {'Friday': 77807,  'Saturday': 88049,  'Monday': 107364,  'Sunday': 118883,  '<newparagraph>Munday': 129548}

  my_months = {'June': 2966,  'March': 3227,  'October': 3267,  'December': 3294,  'January': 3409,  'July': 3704,  'February': 3894,  'August': 3917, 'June.': 47809,  'October.': 49996,  '<newparagraph>March': 51816,  'July.': 53436, '5555-June': 65255, 'February.': 71605,  'September-October': 71751, 'December.': 75879,  'Auguste': 79177,  'January.': 80506,  'May-June': 87982,  '<newparagraph>Augustin': 89141,  '5555-March': 90488,  'Augusto': 92753,  '5555-January': 94730,  'August.': 104009,  'August-September': 107869,  'August-October': 108919,  'August-October': 108919,  'Augustini': 111443,  'June-July': 112725,  'June-July': 112725,  '55-June': 115108,  'mid-August': 125273,  '<newparagraph>Augusto': 126527,  '.<newparagraph>March': 127702,  '<newparagraph>Juneja': 135138}

  my_monthsClean = {'June': 2966,  'March': 3227,  'October': 3267,  'December': 3294,  'January': 3409,  'July': 3704,  'February': 3894,  'August': 3917, 'June.': 47809,  'October.': 49996,  '<newparagraph>March': 51816,  'July.': 53436, '5555-June': 65255, 'February.': 71605,  'September-October': 71751, 'December.': 75879,  'Auguste': 79177,  'January.': 80506,  'May-June': 87982, '5555-March': 90488, '5555-January': 94730,  'August.': 104009,  'August-September': 107869,  'August-October': 108919,  'August-October': 108919, 'June-July': 112725,  'June-July': 112725,  '55-June': 115108,  'mid-August': 125273,  '<newparagraph>Augusto': 126527,  '.<newparagraph>March': 127702}

  my_bodyparts = {'head': 519,  'neck': 531,  'breast': 705,  'eye': 1183,  'hand': 1218,  'knee': 1228,  'ear': 1242,  'hip': 1318,  'back': 1378,  'hair': 1498,  'shoulder': 1509,  'tongue': 1848,  'lip': 1978,  'wrist': 2039,  'elbow': 2171,  'nose': 2217,  'arm': 2248,  'mouth': 2279,  'ankle': 2371,  'leg': 2933,  'thumb': 3029,  'finger': 3102,  'forehead': 3706,  'thigh': 5303,  'toe': 5785,  'cheek': 6754,  'chin': 7565,  'belly': 9741,  'eyebrow': 10967,  'buttocks': 13680,  'nostril': 14744,  'waist': 17552,  'fist': 28296}

  my_jobs = {'Physicians': 239, 'Physician': 711, 'PhysiciansDisclosure': 3046, 'Radiologists': 3349, 'GP': 3819,  'Physicians-American': 4279,  'Radiologist': 4332,  'Occupational': 4586, 'Practical': 8573,  'Physicians<newparagraph>Disclosure': 8843,  'GP.': 9234, 'Physicians.': 11698, 'RadiologistsDisclosure': 16996, 'Doctors': 24000, 'Doctor': 28449,  'Physician-in-Chief': 28457, '<newparagraph>Occupational': 32055,  '<newparagraph>Physicians': 35433, 'PhysiciansDaniel': 41151, 'PharmacyDisclosure': 42670, 'PhysiciansNeil': 46424,  'Radiologists<newparagraph>Disclosure': 46690, '<newparagraph>Psychiatric': 50295, 'Physician-In-Charge': 55936, 'PhysicianJ': 62872, 'PhysiciansRobin': 64618, 'Radiologist-in-Chief': 65716, 'Physician<newparagraph>Kilbourn': 69547,  'SurgicalDavid': 70174, 'RadiologistsUdo': 72610, '<newparagraph>Doctor': 74374, 'PhysicianKilbourn': 75970,  'Neuro-Radiologist': 87642,  'patients.<newparagraph>Clinical': 88910,  '<newparagraph>Physician': 89069,  'Veterinarians': 93932,  'Therapists': 94445, 'PhysiciansGlen': 110887, 'Radiologist.': 117604,  'RadiologistsDjamil': 118864, 'Physician/Neurologist': 127214, 'patients.<newparagraph>Surgical': 130852, '<newparagraph>Radiologists': 136049, 'Veterinarians.': 143988}

  #my = {'the': 3}

  groups = [my_days, my_months, my_monthsClean, my_bodyparts, my_jobs]

  glabels = ["days", "months_wide", "months", "bodyparts", "jobs"]

