import numpy as np
import mdp
import matplotlib.pyplot as plt

def check_lines(method, filenames, words):
	for fn in filenames: #no need for iteration since all files have same terms
		count = 0;count_no = 0
		common = '';uncommon = ''
		with open(fn, 'r') as data:
			for line in data:
				#print line.split(" ")[0]
				if line.split(" ")[0].strip() in words:
					#print line.split(" ")[0].strip()
					count+=1
					common+=line
				else:
					#print "#### "+line.split(" ")[0].strip()
					count_no+=1
					uncommon+=line
		print fn
		print "words that are in the dataset: "+str(count)+" ("+str(count*100/count_no)+"%)"
		print "words that are NOT in the dataset: "+str(count_no)+" ("+str(100-(count*100/count_no))+"%)"

		# write filtered terms in new files
		f_common = open(fn+'common', 'w+')
		f_common.write(common)
		f_common.close()

		f_uncommon = open(fn+'uncommon', 'w+')
		f_uncommon.write(uncommon)
		f_uncommon.close()
	return


def filter_by_term(terms):
	import nltk
	from nltk.corpus import wordnet
	#import enchant
	from nltk.corpus import words

	#engDict = enchant.Dict("en_US")

	#########################
	## List of NLTK tagset() in 5 groups:
	# Adjective:
	g0 = ["JJ", "JJR", "JJS"]
	# OTHER:
	g1 = ["PRP", "PRP$", "WP", "WP$", "DT", "WDT","CC", "IN", "RB", "RBR", "RBS", "WRB"]
	# Nouns:
	g2 = ["NN", "NNS"]
	# Noun proper:
	g3 = ["NNP", "NNPS"]
	# Symbols, Numbers:
	g4 = ["$", "''", "(", ")", ",", "--", ".", ":", "CD", "EX", "FW", "LS", "PDT", "POS", "RP", "SYM", "TO", "UH", "``"]
	# Verbs:
	g5 = ["MD", "VBD", "VBG", "VBN", "VBP", "VBZ"]

	total = 0
	count_0 = 0;count_1 = 0;count_2 = 0;count_3 = 0;count_4 = 0;count_5 = 0;count_6 = 0;
	str_w5='';str_eng='';str_0 = '';str_1 = '';str_2 = '';str_3 = '';str_4 = '';str_5 = '';str_6 = '';str_not_counted=''
	not_counted = 0
	w5 = 0;eng = 0
	mycol = '';col_eng = '';col_w5='';col_g=''
	mynewterms = '';
	myhead = 'term\tEnglish\tw5000\tPoS\n'
	for line in terms:
		total+=1
		#print str(total)
		token = line.split()[0].strip()

		# Check word kind:
		kind = nltk.pos_tag([token])[0][1]

		mycol = token+'\t'
		if token in words.words():
			eng+=1
			str_eng+=line
			mycol += '1\t'
		else:
			mycol += '0\t'
		# Check 5000 most common English words
		if token in words5:
			w5+=1
			str_w5+=line
			mycol += '1\t'
		else:
			mycol += '0\t'
		# switch:
		if kind in g0:
			count_0+=1
			str_0+=line
		elif kind in g1:
			count_1+=1
			str_1+=line
		elif kind in g2:
			if wordnet.synsets(token.decode('utf-8')):
				count_2+=1
				str_2+=line
			else:
				count_6+=1
				str_6+=line
				#print token+" | ",
		elif kind in g3:
			count_3+=1
			str_3+=line
		elif kind in g4:
			count_4+=1
			str_4+=line
		elif kind in g5:
			count_5+=1
			str_5+=line
		else:
			not_counted+=1
			str_not_counted+=line
			kind = 'NaN'
		mycol += kind+'\n'
		print mycol
		#print "-----------"
		mynewterms += mycol
	mynewterms = myhead+mynewterms
	print
	print "TOTAL terms1 = "+str(total)
	print "TOTAL terms2 = "+str(count_0+count_1+count_2+count_3+count_4+count_5+count_6+not_counted)
	print "Words in 5000 english words: "+str(w5)
	print "Words in English: "+str(eng)
	print "0_Adjective: "+str(count_0)+" ("+str(count_0*100/total)+"%)"
	print "1_    Other: "+str(count_1)+" ("+str(count_1*100/total)+"%)"
	print "2_    Nouns: "+str(count_2)+" ("+str(count_2*100/total)+"%)"
	print "3_ Proper N: "+str(count_3)+" ("+str(count_3*100/total)+"%)"
	print "4_  symbols: "+str(count_4)+" ("+str(count_4*100/total)+"%)"
	print "5_    Verbs: "+str(count_5)+" ("+str(count_5*100/total)+"%)"
	print "6_    Noise: "+str(count_6)+" ("+str(count_6*100/total)+"%)"
	print "Not counted: "+str(not_counted)

	dataset_name = "THYMES"
	for n in ["0", "1","2","3","4","5","6","w5","not_counted"]: #range(1,6):
		# write filtered terms in new files
		name0 = 'token-kind_'
		f = open("filtered_terms/"+dataset_name+name0+"-"+str(n), 'w+')
		f.write(eval('str_'+str(n)))
		f.close()

	f = open("filtered_terms/"+dataset_name+"-with-extra.txt", 'w+')
	f.write(mynewterms)
	f.close()

	return

#################################################
# Loading 5000 most frequent Engl;ish words from http://www.wordfrequency.info/top5000.asp
words5 = []
words_list = open('external-data/5000words.txt').readlines()
for w in words_list:
	words5.append(w.replace('\n',''))


my_terms_list = []
filename = '../DATASET-4-word2vec-Tuur-DATA-2/thyme.220k.w1.sg.bin.txt'
with open(filename, 'r') as tensor:
	for line in tensor:
		my_terms_list.append(line.split(" ")[0])
		#print line.split(" ")[0]


filter_by_term(my_terms_list)

'''
GLAUCOMA:
TOTAL terms1 = 38726
TOTAL terms2 = 38726
Words in 5000 english words: 3085
Words in English: 10962
0_Adjective: 4354 (11%)
1_    Other: 872 (2%)
2_    Nouns: 11504 (29%)
3_ Proper N: 0 (0%)
4_  symbols: 2150 (5%)
5_    Verbs: 2495 (6%)
6_    Noise: 17264 (44%)
Not counted: 87


THYMES:
TOTAL terms1 = 3466
TOTAL terms2 = 3466
Words in 5000 english words: 1022
Words in English: 1862
0_Adjective: 365 (10%)
1_    Other: 295 (8%)
2_    Nouns: 1803 (52%)
3_ Proper N: 81 (2%)
4_  symbols: 240 (6%)
5_    Verbs: 346 (9%)
6_    Noise: 297 (8%)
Not counted: 39

'''
