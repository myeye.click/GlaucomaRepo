import os

path_in = "../DATASET-4-word2vec/"

# get terms in englis dictionary:
english = "terms_english.txt"
en_terms = []
with open(english) as terms:
	for term in terms.readlines():
		en_terms.append(term.split(",")[0])

#f = open("terms-only-list-english.txt", 'w+')
#f.write("\n".join(en_terms))
#f.close()

def filter_terms_by_dict(path_in, filenames, en_terms):
	path_out = "../DATASET-4-word2vec-filtered/"
	for filename in filenames:
		# Filter word2vec by en_terms
		new = ''
		with open(path_in+filename) as terms:
			for term in terms.readlines():
				if term.split(" ")[0] in en_terms:
					 new += term
					 #print term.split(" ")[0],
		print "################################"
		#print new[:1000]
		#print str(len(new))

		## Save new list
		f = open(path_out+filename, 'w+')
		f.write(new)
		f.close()
		print "Saved: "+path_out+filename
	return

#filenames = os.listdir(path_in)
#filter_terms_by_dict(path_in, filenames, en_terms)
