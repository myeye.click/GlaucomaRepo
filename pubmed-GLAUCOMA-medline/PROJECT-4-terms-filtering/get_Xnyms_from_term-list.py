from nltk.corpus import wordnet as wn
from itertools import chain

word = "house"
print "hello"
for i,j in enumerate(wn.synsets(word)):
    print "Meaning",i, "NLTK ID:", j.name()
    print "Hypernyms:", ", ".join(list(chain(*[l.lemma_names() for l in j.hypernyms()])))
    print "Hyponyms:", ", ".join(list(chain(*[l.lemma_names() for l in j.hyponyms()])))
    print "Definition:",j.definition()
    print
