import codecs
import sys
from nltk import bigrams
import collections

def gen_ML_Bigram(text):
	texfbig = codecs.open(text,'r','utf8').read()
	tokens = texfbig.split()
	ml_bigram = sorted(bigrams(tokens))
	#out = codecs.open("ml_bigram.txt",'w','utf8')
	#for ml in ml_bigram:
	#		out.write(" ".join(ml))
	#		out.write("\n")
	my = collections.Counter(ml_bigram).most_common()
	for i in my:
		i = list(i)
		i[0] = list(i[0])
		if i[1]>1:
			print i
	return


file = sys.argv[1]
gen_ML_Bigram(file)
