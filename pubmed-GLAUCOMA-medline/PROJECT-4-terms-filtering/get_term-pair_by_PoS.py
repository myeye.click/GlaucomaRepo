from nltk.corpus import wordnet as wn
import glob, re

'''
from URL: http://www.nltk.org/howto/wordnet.html

Synsets:


Lemmas:
wn.synset('man.n.01').lemmas()[0].antonyms()

'''


def get_terms_by(kind2, kind1):
	## Load the listof terms with extra info:

	## For Glaucoma dataset:
	###myfile = 'filtered_terms/mc5-with-extra.txt'

	## For THYMES dataset:
	#myfile = 'filtered_terms/THYMES-terms-list.txt'
	myfile = 'filtered_terms/THYMES-with-extra.txt'
	with open(myfile, 'r') as myTerms:
		_kind1 = []
		_kind2 = []
		for term in myTerms:
			#print term
			if "\t1\t" in term:
				if kind1+"\n" in term:
					_kind1.append(term)
				elif kind2+"\n" in term:
					_kind2.append(term)

	## sort alphabetically:
	s_kind1 = sorted(_kind1)
	s_kind2 = sorted(_kind2)

	#print "HEY"
	#print str(s_kind1)
	#print "######"
	#print str(s_kind1)
	#f = open("filtered_terms/NN_sorted.txt", 'w+')
	#f.write("\n".join(s_NN))
	#f.close()

	#f = open("filtered_terms/NNS_sorted.txt", 'w+')
	#f.write("\n".join(s_NNS))
	#f.close()

	s_kind1_kind2 = []
	for mykind2 in s_kind2:
		my = mykind2.split("\t")[0].strip()
		for mykind1 in s_kind1:
			if my in mykind1 and my[:2] in mykind1[:2] and len(my)>3:
				out = my+"  -->  "+mykind1.split("\t")[0].strip()
				#s_NN_NNS.append(out)
				print out

	#f = open("filtered_terms/NNS-and-NN_sorted.txt", 'w+')
	#f.write("\n".join(s_NN_NNS))
	#f.close()

####################################################
print
print "#############################       NN in NNS"
print
get_terms_by("NN", "NNS")
print
print "#############################       NN in VBG"
print
get_terms_by("NN", "VBG")
print
print "#############################       NN in VBD"
print
get_terms_by("NN", "VBD")
print
print "#############################       NN in MD"
print
get_terms_by("NN", "MD")
print
print "#############################       MD in NN"
print
get_terms_by("MD", "NN")
print
print "#############################       NN in VBN"
print
get_terms_by("NN", "VBN")
print
print "#############################       NN in VBP"
print
get_terms_by("NN", "VBP")
print
print "#############################       NN in VBZ"
print
get_terms_by("NN", "VBZ")
print
print "#############################       JJ in JJR"
print
get_terms_by("JJ", "JJR")
print
print "#############################       JJ in JJS"
print
get_terms_by("JJ", "JJS")



#get_terms_by("", "")
