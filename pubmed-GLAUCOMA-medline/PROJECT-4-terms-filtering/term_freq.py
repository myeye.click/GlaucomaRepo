from __future__ import division
import glob, re
import nltk
from nltk.corpus import PlaintextCorpusReader

# Load corpus
files = ".*\.txt"
corpusPath = "../DATASET-3-records-medline-with-abs-CLEAN/"
corpus0 = PlaintextCorpusReader(corpusPath, files)
corpus  = nltk.Text(corpus0.words())
len_corpus = len(corpus)

# List of terms
termsFile = "terms-list-57025.txt"
termList = []
newTerms = ""
c = 0
with open(termsFile, 'r') as lines:
	for line in lines.readlines():
		c+=1
		#if c <10:
		#freq1 = str(100*corpus.count())+" | "+str(len_corpus)
		l = re.sub('^[\(,.;{]|[%;.>,:\)]$', '', line.rstrip())
		freq = 100*(corpus.count(l)/len_corpus)
		print c,l, freq
		#termList.append([line.strip(),freq])
		newTerms += line.strip()+"\t"+str(freq)+"\n"

#print "num of terms: "+str(len(termList))
print "################"
#print termList

## Save new list
f = open('terms_with_freq.txt', 'w+')
f.write(str(newTerms))
f.close()
