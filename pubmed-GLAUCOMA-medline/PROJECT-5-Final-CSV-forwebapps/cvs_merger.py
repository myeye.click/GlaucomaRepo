import pandas as pd
import os

### Methods:
def terms_and_vals_csv(path_in, path_out, termList):
	for filename in os.listdir(path_in):
		vectorList = pd.read_csv(path_in+filename, sep=',', header=None)
		vectorList.columns = ["x", "y"]
		myJoin = termList.join(vectorList)
		print "to path -> "+path_out
		myJoin.to_csv(path_out+filename, index=False)
		print "Saved -> "+filename

	print "... done!"
	return

def filter_tensor_by_term(path_in, path_out, filenames, termListExtra, myFilter, column):
	# Get path_in files -> termListExtra
	#filename = "pca2-medline-with-abs-w8-s80-mc5-tensor.txt"
	for filename in filenames:
		# Getting 2D vectors with terms :
		vectorList = pd.read_csv(path_in+filename, sep=',') # terms,x,y

		# Get terms with frequencies -> freqList
		freqList = pd.read_csv("terms_with_freq.txt", sep=',') # terms,freq

		termListExtra = pd.read_csv("mc5-with-extra.txt", sep=",")
		print "> terFullExtra: "
		#print termListExtra[:10]
		print termListExtra.shape

		# join dataframes: vectorList and freqList
		termFullList0 = pd.merge(vectorList,freqList)
		termFullList = pd.merge(termFullList0,termListExtra)
		print "> termFullList:"
		#print termFullList[:10]
		print termFullList.shape

		# Filters defined:
		myval = myFilter[column]

		# ~~~~ REQUIRES EDITION ~~~~ #
		# Apply myFilter to termListExtra
		# For numerical conditions
		#termsFiltered = termFullList.loc[termFullList[column] == int(myval)]
		# For non-numeric conditions
		termsFiltered = termFullList.loc[termFullList[column] == myval]

		print "> termsFiltered by "+column+" == "+myval
		#print termsFiltered[:10]
		print termsFiltered.shape

		# Save new list. Extend file name with "f-"+ active filter
		myfilename = path_out+"filter-"+column+"-"+myval+"-"+filename
		termsFiltered.to_csv(myfilename, index=False)
		print "Saved -> "+myfilename
		print
		print "~~~~~~~~~~~~~~~~"
	return

#########################################################
### Preparatuon and Calls:

###  Calls:
## Call tsne
path_in_tsne = "../DATASET-5-tsne/"
path_out_tsne = "../DATASET-5-tsne-term-merged/"

termList = pd.read_csv("terms-only-list-english.txt", header=None)
termList.columns = ["terms"] # Add column label to the pandas dataframe

terms_and_vals_csv(path_in_tsne, path_out_tsne, termList)

## Call pca
'''
path_in = "../DATASET-6-PCA2/"
path_out = "../DATASET-7-PCA2-terms-meged/"

# Getting filenames list to be used to call methods with:
termList = pd.read_csv("terms-list-57025.txt", header=None)
termList.columns = ["terms"] # Add column label to the pandas dataframe

filenames = os.listdir(path_in2)
print filenames
# terms_and_vals_csv(path_in, path_out, termList)
myFilter= {"terms":"", "english":"1", "w5000":"1", "pos":"NN"}
column = 'pos'
filter_tensor_by_term(path_in2, path_out2, filenames, termList, myFilter, column)
'''
