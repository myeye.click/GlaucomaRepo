
def avg(list):
	sum = 0
	for elm in list:
			sum += float(elm)
	print "The average element of the list is: " + str(sum/(len(list)*1.0))

def min(list):
	min = list[0]
	for elm in list[1:]:
		if elm < min:
			min = elm
	print "The minimum value in the list is: " + str(min)

def max(list):
	max = list[0]
	for elm in list[1:]:
		if elm > max:
			max = elm
	print "The maximum value in the list is: " + str(max)


def median(list):
	import numpy
	print numpy.median(list)


######################################


c=0
element=''
list = []
#with open("terms_with_freq.txt", "r") as data:
with open("../PROJECT-6-Test-Results--Viz_Analysis/test-deree.csv", "r") as data:
	for d in data.readlines():
		if c>0:
			#element = d.strip().split(",")[2]
			element = d.strip().split(",",1)[1]
			if element == "0.0":
				element = 0
			list.append(float(element))
		c+=1
	print "-- list: num of elements: "+str(c)

# Change every element of the list to a numeric type.
#import numpy as np
#list = np.array(list).astype(np.float)

print "First element of the list: "+str(list[0])
#print list[1]

print "--"
print avg(list)
print "--"
print min(list)
print "--"
print max(list)
print "--"
print "Median:"+str(median(list))
