import numpy as np
import os, math

### Methods:
def cart2pol(term, x, y):
	rho = np.sqrt(x**2 + y**2)
	phi = math.degrees(np.arctan2(y, x))
	i = term+","+str(rho)+","+str(+phi)
	return(i)

def pol2cart(rho, phi):  # Not used
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

def coor2complex(path_in, path_out):
	for filename in os.listdir(path_in):
		with  open(path_in+filename, 'r') as tensor_xy:
			tensor_i = []
			for vector in tensor_xy.readlines()[1:]:
				v = vector.split(",")
				tensor_i.append(cart2pol(v[0], float(v[1]), float(v[2])))
			tensor_i = ["terms,x,y"] + tensor_i
			print "to path -> "+path_out
			tensor_i = '\n'.join(tensor_i)
			f = open(path_out+filename, "w")
			f.write(str(tensor_i))
			f.close()
			print "Saved -> "+filename
	print "... done!"
	return

#########################################################
### Preparatuon and Calls:

###  Call 1:
path_in_tsne = "../DATASET-5-tsne-term-merged//"
path_out_tsne = "../DATASET-5-tsne-term-merged-i/"
coor2complex(path_in_tsne, path_out_tsne)
