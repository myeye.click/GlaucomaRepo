groups = {}
#############################
#NN in NNS

groups["nn_nns"] = [["base": " bases"],["bowel": " bowels"],["check": " checked"],["effect": " effects"],["gene": " genetics"],["need": " needs"],["remain": " remains"], ["term": " terms"],["time": " times"],["work": " works"]]

#############################
#NN in VBG

groups["nn_vbg"] = [["concern": " concerning"],["extend": " extending"],["feel": " feeling"],["form": " forming"],["learn": " learning"],["look": " looking"],["proceed": " proceeding"],["regard": " regarding"],["remain": " remaining"],["rest": " resting"],["reveal": " revealing"],["screen": " screening"],["show": " showing"],["stain": " staining"],["start": " starting"],["suggest": " suggesting"],["tattoo": " tattooing"],["test": " testing"],["undergo": " undergoing"],["understand": " understanding"],["walk": " walking"],["work": " working"]]

###################################
#NN in VBD
#NN in VBN

groups["nn_vbd_vbn"] = [["agree": " agreed"], ["decide": " decided"], ["include": " included"], ["reveal": " revealed"], ["side": " sided"], ["stage": " staged"], ["stop": " stopped"], ["assess": " assessed"], ["base": " based"], ["confirm": " confirmed"], ["determine": " determined"], ["extend": " extended"], ["form": " formed"], ["increase": " increased"], ["inform": " informed"], ["involve": " involved"], ["note": " noted"], ["order": " ordered"], ["outline": " outlined"], ["provide": " provided"], ["receive": " received"], ["repeat": " repeated"], ["require": " required"], ["return": " returned"], ["smoke": " smoked"], ["state": " stated"], ["suspect": " suspected"], ["work": " worked"]]

#############################
#JJ in JJR
#JJ in JJS

groups["jj_jjr_jjs"] = [["great": " greater"],["high": " higher"],["small": " smaller"],["great": " greatest"]]


#############################
# bigrams
groups["bigrams"] = []
