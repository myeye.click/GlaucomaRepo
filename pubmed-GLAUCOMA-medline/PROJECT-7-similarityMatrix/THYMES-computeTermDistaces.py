import numpy as np
import scipy
import scipy.spatial

# two vestors distance:
def getDistance(a, b, method):
	pdist = scipy.spatial.distance.pdist
	T = [a,b]
	return pdist(T, method)[0]

def computeDistances(path, path2, filename, pairTerms, kind, format):
	# Get vectors from tensors that are in pairTerms
	myLines = []
	out2report = ""
	out2file = ""
	out2csv = ""
	with open(path+filename) as data:
		c=0
		for line in data:
			term = line.split(" ")[0]
			vector = line.split(" ")[1:]
			for check in pairTerms:
				if term in check:
					myLines.append(line.split(" ")[:-1])
	myDistances = []
	out2report += "]n~~~~~~~~~~~~~~~~~~~~~~\n"
	out2report += "Not-found terms: "
	out_tmp = []
	out_tmp2 = []
	out_tmp3 = ""
	for check in pairTerms:
		v0 = []
		v1 = []
		for my in myLines:
			if my[0] == check[0]:
				v0 = my[1:]
			if my[0] == check[1]:
				v1 = my[1:]
		if len(v0)>0 and len(v1)>0:
			myDist = getDistance(v0, v1, 'euclidean')
			#print ">> Distance "+check[0]+" -> "+check[1]+" = "+str(myDist)
			myDistances.append([check[0], check[1], myDist])
		else:
			out2report +=  " !! Probems with distance: "+check[0]+" -> "+check[1]
			out_tmp.append([check[0], check[1]])

	#for m in myDistances:
	#	print m
	out2report += "\n~~~~~~~~~~~~~~~~~~~~~~\n\n"
	out2report += "Results: "
	# Sort list by distance value:
	myDistances.sort(key=lambda x: x[2])
	for m in myDistances:
		out_tmp2.append(m)
		out2report += str(m)+"\n"
		out_tmp3 += str(m[0])+","+str(m[1])+","+str(m[2])+"\n"
	out2report += "\n"
	out2file += "\npairs = "+str(out_tmp2)
	out2file += "\nnotFoundTerms = "+str(out_tmp)
	out2csv += "## group: "+kind+"\n"+out_tmp3+"\n"

	#############
	## Write out
	#
	#with open(path2+'PYLIST/Distances-'+filename, "w") as myfile:
	#	myfile.write(out2file)
	with open(path2+'THYMES-CSV/byKind/THYMES-'+kind+'-Distances-'+filename+".csv", 'w+') as myfile:
		myfile.write(out2csv)
	out2csv = ""
	###print out2report

	return

#get lists of term pairs grouped by POS
import PAIRED_TERMS as pairs
groups = pairs.groups

def listOfPairTerms(grp):
	pairTerms = []
	#pairTerms = pairs.jj_jjr_jjs
	pairTerms = groups[grp]
	return pairTerms

###################################################
###################################################

# open tensors of terms
path = "../DATASET-4-word2vec-Tuur-DATA-2/" ## from word2vec

# wgere to save the computed distaces:
path2 = '../DATASET-9-similarityMatrix/'

# Set distance method:
##https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.pdist.html#scipy.spatial.distance.pdist
methods = ['euclidean', 'cosine', 'minkowski']
format = methods[0]

##  Load the list pf pairs to compute

# List of files to calculate from
from os import walk
tmp = []
files = []
for (dirpath, dirnames, filenames) in walk(path):
    tmp.extend(filenames)
    break

for f in tmp:
    #print f, f[-10:]
    if f[:10] == "thyme.220k":
        files.append(f)

for i in ["nn_nns", "nn_vbg", "jj_jjr_jjs", "nn_vbd_vbn"]: ## , "bigrams"]:
	pairTerms = listOfPairTerms(i)
	kind = i
	for f in files:
		print
		print "####################################################"
		print "group: "+i
		print "processing file: "+f
		computeDistances(path, path2, f, pairTerms, kind, format)
