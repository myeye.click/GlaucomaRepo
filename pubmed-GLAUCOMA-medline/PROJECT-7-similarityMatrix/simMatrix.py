import numpy as np
import scipy
import scipy.spatial

# two vestors distance:
def myDist(a, b, method):
	pdist = scipy.spatial.distance.pdist
	T = [a,b]
	return pdist(T, method)[0]

def generateSimMatrix(path, path2, filename, format):
	print "File: "+filename
	with open(path+filename) as data:
		num_columns = len(data.readlines()[0].strip().split(" "))
		num_rows = sum(1 for line in open(path+filename))
		print " - num_columns --> "+str(num_columns-1)
		print " - num rows --> "+str(num_rows-1)

		r = range(1, num_columns)
		c = 1
		import os
		if num_columns > 4:
			terms = np.loadtxt(path+filename, dtype="str", usecols=[0])
			vectors = np.loadtxt(path+filename, usecols=(r))
			#print "... shape vectors: "+str(vectors.shape)
			#print "... shape terms: "+str(terms.shape)

		# Iterate through vectors and build simMatrix:
		c=0
		for i in range(num_rows-1):
			myMatrix = ""
			for j in range(i, num_rows-1):
				c+=1
				#myMatrix += str(i)+terms[i]+" "+str(j)+terms[j]+" "+str(myDist(vectors[i], vectors[j], 'euclidean'))+"\n"
				myMatrix += str(i)+" "+str(j)+" "+str(myDist(vectors[i], vectors[j], 'euclidean'))+"\n"
				#print i,j
				#print "distance: "+str(myDist(vectors[i], vectors[j], 'euclidean'))
			with open(path2+'SimMatrix-'+filename, "a") as myfile:
				myfile.write(myMatrix)

		print ">>>> number of simMatrix elements iXj :"+str(c)

	print
	return

#### def buldSimMatrix

# open tensor of terms (from word2vec)
path = "../DATASET-4-word2vec-filtered/"

path2 = '../DATASET-9-similarityMatrix/'
format = 'txt'

from os import walk
tmp = []
files = []
for (dirpath, dirnames, filenames) in walk(path):
    tmp.extend(filenames)
    break

for f in tmp:
    #print f, f[-10:]
    if f[-10:] == "tensor.txt":
        #print ">>> "+f
        files.append(f)

for f in files[1:3]:
	print "~~~~~~~~"
	print "processing file: "+f
	generateSimMatrix(path, path2, f, format)

'''
notes:
Size of the matrix: 13286 x 13286
Num elements of half a matrix + diagonal: 13287*(13287 + 1)/2

-> (13287*13288)/2 = 88278828

'''

# separate terms (indexed) from vectors

# a) from terms index: define linguistic groups of terms

# save terms index

# save linguistic groups of terms

# b) From the vectors, build SimMatrix

# Save SimMatrix


#### def checkTermSim(list_of_terms)

# Open terms index and SimMatrix

# get vectors (distances) for the list_of_terms

# Output values: Visualize results
 	# [??] maybe the app can produce html/js code and open it in a browser using
